#pragma once
#include <string>
#include "../common/ScpiUtil.h"
#include <tuple>
#include <any>
#include <optional>
#include <variant>
#include <cstring>
#include <iostream>

inline bool ends_with(std::string const & value, std::string const & ending)
{
    if (value.length() >= ending.length())
    {
	return (0 == value.compare (value.length() - ending.length(), ending.length(), ending));
    }
    else
    {
	return false;
    }
}

inline bool first_word_is(std::string const & value, std::string const & start)
{
	std::string line = value;
	if(ends_with(line, "?")) { line.pop_back(); }

	size_t pos = line.find(' ');
	if(pos == std::string::npos) //if only one word
	{
		return line == start;
	}
	std::string firstWord = line.substr(0, pos);
	return firstWord == start;
}

std::string get_last_word(std::string s) {
  auto index = s.find_last_of(' ');
  std::string last_word = s.substr(++index);
  return last_word;
}

bool is_number(const std::string& s)
{
    return( strspn( s.c_str(), "-.0123456789" ) == s.size() );
}
bool is_int(const std::string& s)
{
    return( strspn( s.c_str(), "-0123456789" ) == s.size() );
}


std::tuple<CommandType::Enum, ScpiData::Enum, std::any, bool> parseLine(const std::string& incoming)
{
    std::cout << "\tparsed: ";

    CommandType::Enum cmdType;
    ScpiData::Enum cmd;
    std::any setRequestVal;

	if(first_word_is(incoming, ":VOLT"))	 { std::cout << "[data: :VOLT]"; cmd = ScpiData::VoltageLimit; }
	else if(first_word_is(incoming, ":CURR"))	 { std::cout << "[data: :CURR]"; cmd = ScpiData::CurrentLimit; }
	else if(first_word_is(incoming, "SYST:SET"))	 { std::cout << "[data: SYST:SET]"; cmd = ScpiData::ControlMode; }
	else if(first_word_is(incoming, "OUTP:PON"))	 { std::cout << "[data: OUTP:PON]"; cmd = ScpiData::StartMode; }
	else if(first_word_is(incoming, ":VOLT:PROT:LEV"))  { std::cout << "[data: :VOLT:PROT:LEV]"; cmd = ScpiData::OverVoltageLimit; }
	else if(first_word_is(incoming, ":VOLT:LIM:LOW"))   { std::cout << "[data: :VOLT:LIM:LOW]"; cmd = ScpiData::UnderVoltageLimit; }
	else if(first_word_is(incoming, ":CURR:PROT:STAT")) { std::cout << "[data: :CURR:PROT:STAT]"; cmd = ScpiData::FoldbackProtection; }
	else if(first_word_is(incoming, "OUTP:STAT"))	 { std::cout << "[data: OUTP:STAT]"; cmd = ScpiData::OutputEnable; }
	else if(first_word_is(incoming, "MEAS:VOLT"))	 { std::cout << "[data: MEAS:VOLT]"; cmd = ScpiData::OutputVoltage; }
	else if(first_word_is(incoming, "MEAS:CURR"))	 { std::cout << "[data: MEAS:CURR]"; cmd = ScpiData::OutputCurrent; }
	else if(first_word_is(incoming, ":VOLT:PROT:TRIP")) { std::cout << "[data: :VOLT:PROT:TRIP]"; cmd = ScpiData::OverVoltageTripped; }
	else if(first_word_is(incoming, ":CURR:PROT:TRIP")) { std::cout << "[data: :CURR:PROT:TRIP]"; cmd = ScpiData::FoldbackTripped; }
	else if(first_word_is(incoming, "SOUR:MOD")) { std::cout << "[data: SOUR:MOD]"; cmd = ScpiData::OperatingMode; }

	else
	{
		std::cerr << "unknown command: " << incoming << std::endl;
		return {cmdType, cmd, setRequestVal, false};
	}

    if(ends_with(incoming, "?"))
    {
		cmdType = CommandType::Get;
		std::cout << "[get cmd] ";
    }
	else
	{
		cmdType = CommandType::Set;
		std::cout << "[set cmd] ";

		std::string valRequest = get_last_word(incoming);
		if(is_number(valRequest))
		{
			if(is_int(valRequest))
			{
				int intVal = ::atoi(valRequest.c_str());
				setRequestVal = intVal;
				std::cout << "[val int:  " << intVal << "]";
			}
			else
			{
				double dVal = ::atof(valRequest.c_str());
				setRequestVal = dVal;
				std::cout << "[val double:  " << dVal << "]";
			}

		}
		else
		{
			setRequestVal = valRequest;
			std::cout << "[val str:  " << valRequest << "]";
		}

    }

    std::cout << std::endl;
	return {cmdType, cmd, setRequestVal, true};
}

