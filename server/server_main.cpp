#include <evpp/tcp_server.h>
#include <evpp/buffer.h>
#include <evpp/tcp_conn.h>
#include <evpp/timestamp.h>
#include <glog/logging.h>
#include "ParseScpiLine.h"
#include "SimulatedHardware.h"

static SimulatedHardware simhardware;

int main(int argc, char* argv[])
{
	std::string addr = "0.0.0.0:9099";

	std::cout << "starting server on: " << addr << std::endl;

	uint32_t thread_num = 4;
	evpp::EventLoop loop;

	evpp::TCPServer server(&loop, addr, "SimScpiServer", thread_num);
	server.SetMessageCallback([](const evpp::TCPConnPtr& conn, evpp::Buffer* msg)
	{
		std::string msgstr(msg->NextAll().data());
		std::stringstream ss(msgstr);
		std::string line;
		while(std::getline(ss, line, '\n'))
		{
			if(line.empty()) continue;

			std::cout << "\nreceived: " << line << std::endl;

			std::tuple<CommandType::Enum, ScpiData::Enum, std::any, bool> parsedLine = parseLine(line);

			if(std::get<bool>(parsedLine) == true)
			{
				std::optional<std::string> responce = simhardware.receiveCommand(parsedLine);

				if(responce.has_value())
				{
					std::cout << "\tsending reply: " << responce.value() << std::endl;
					conn->Send(responce.value());
				}
			}
		}
	});

	server.SetConnectionCallback([](const evpp::TCPConnPtr& conn) {
		if (conn->IsConnected()) {
			LOG_INFO << "A new connection from " << conn->remote_addr();
		} else {
			LOG_INFO << "Lost the connection from " << conn->remote_addr();
		}
	});
	server.Init();
	server.Start();
	loop.Run();
	return 0;
}
