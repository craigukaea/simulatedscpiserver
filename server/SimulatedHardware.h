#pragma once
#include <tuple>
#include <any>
#include <optional>
#include <variant>
#include "../common/ScpiUtil.h"
#include <functional>
#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>

class SimulatedHardware
{
public:

    SimulatedHardware()
    {
	//This is where we define the behaviour of the simulated hardware
	thread = std::thread([this]()
	{
	    while(true)
	    {
			std::this_thread::sleep_for(std::chrono::milliseconds(100));
			if(outputEnable == true)
			{
				if(opperatingMode == OperatingMode::ConstantVoltage)
				{
					double resistance = 10;
					outputVoltage = voltageLimit.load();
					outputCurrent = outputVoltage.load() / resistance;
				}
			}
			else
			{
				outputVoltage = 0;
				outputCurrent = 0;
			}
	    }
	});

    }

	std::optional<std::string> receiveCommand(const std::tuple<CommandType::Enum, ScpiData::Enum, std::any, bool>& cmd)
    {
		std::optional<std::string> responce;

		//For a Set Command
		if(std::get<CommandType::Enum>(cmd) == CommandType::Set)
		{
			std::cout << "\tsetting value..." << std::endl;
			if(!std::get<std::any>(cmd).has_value())
			{
				std::cerr << "No value sent with set command" << std::endl;
				//No responce for a Set command;
				return responce;
			}
			try
			{
				switch(std::get<ScpiData::Enum>(cmd))
				{
					case(ScpiData::VoltageLimit) : voltageLimit = std::any_cast<double>(std::get<std::any>(cmd)); break;
					case(ScpiData::CurrentLimit) : currentLimit = std::any_cast<double>(std::get<std::any>(cmd)); break;
					case(ScpiData::OutputEnable) : outputEnable = std::any_cast<int>(std::get<std::any>(cmd)); break;
					case(ScpiData::OutputVoltage) : outputVoltage = std::any_cast<double>(std::get<std::any>(cmd)); break;
					case(ScpiData::OutputCurrent) : outputCurrent = std::any_cast<double>(std::get<std::any>(cmd)); break;
					case(ScpiData::OperatingMode) : opperatingMode = std::any_cast<OperatingMode::Enum>(std::get<std::any>(cmd)); break;
					case(ScpiData::ControlMode) : controlMode = std::any_cast<ControlMode::Enum>(std::get<std::any>(cmd)); break;
					case(ScpiData::StartMode) : startMode = std::any_cast<StartMode::Enum>(std::get<std::any>(cmd)); break;
					case(ScpiData::OverVoltageLimit) : overVoltageLimit = std::any_cast<double>(std::get<std::any>(cmd)); break;
					case(ScpiData::OverVoltageTripped) : overvoltageTripped = std::any_cast<int>(std::get<std::any>(cmd)); break;
					case(ScpiData::UnderVoltageLimit) : undervoltageLimit = std::any_cast<double>(std::get<std::any>(cmd)); break;
					case(ScpiData::FoldbackProtection) : foldbackProtection = std::any_cast<int>(std::get<std::any>(cmd)); break;
					case(ScpiData::FoldbackTripped) : foldbackTripped = std::any_cast<int>(std::get<std::any>(cmd)); break;
				}
			}
			catch(const std::bad_any_cast& cmd)
			{
				std::cerr << cmd.what() << std::endl;
			}

			//No responce for a Set command;
			return responce;
		}
		else //For a Get Command
		{
			std::cout << "\tgetting value..." << std::endl;

			switch(std::get<ScpiData::Enum>(cmd))
			{
				case(ScpiData::VoltageLimit) : responce = std::to_string(voltageLimit); break;
				case(ScpiData::CurrentLimit) : responce = std::to_string(currentLimit); break;
				case(ScpiData::OutputEnable) : responce = std::to_string(outputEnable); break;
				case(ScpiData::OutputVoltage) : responce = std::to_string(outputVoltage); break;
				case(ScpiData::OutputCurrent) : responce = std::to_string(outputCurrent); break;
				case(ScpiData::OperatingMode) : responce = operatingToStr(opperatingMode.load()); break;
				case(ScpiData::ControlMode) : responce = controlToStr(controlMode.load()); break;
				case(ScpiData::StartMode) : responce = startModeToString(startMode.load()); break;
				case(ScpiData::OverVoltageLimit) : responce = std::to_string(overVoltageLimit); break;
				case(ScpiData::OverVoltageTripped) : responce = std::to_string(overvoltageTripped); break;
				case(ScpiData::UnderVoltageLimit) : responce = std::to_string(undervoltageLimit); break;
				case(ScpiData::FoldbackProtection) : responce = std::to_string(foldbackProtection); break;
				case(ScpiData::FoldbackTripped) : responce = std::to_string(foldbackTripped); break;
			}

			return responce;
		}
    }
private:
	std::atomic<double> voltageLimit = 0;
	std::atomic<double> currentLimit = 0;
    std::atomic<int> outputEnable = 0;
	std::atomic<double> outputVoltage = 0;
	std::atomic<double> outputCurrent = 0;
	std::atomic<OperatingMode::Enum> opperatingMode = OperatingMode::ConstantVoltage;
	std::atomic<ControlMode::Enum> controlMode = ControlMode::Remote;
	std::atomic<StartMode::Enum> startMode = StartMode::SafeStart;
	std::atomic<double> overVoltageLimit = 100;
    std::atomic<int> overvoltageTripped = false;
	std::atomic<double> undervoltageLimit = 0;
    std::atomic<int> foldbackProtection = true;
    std::atomic<int> foldbackTripped = false;
    std::thread thread;
};
