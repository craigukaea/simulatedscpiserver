#include <evpp/tcp_client.h>
#include <evpp/tcp_conn.h>
#include "../common/ScpiUtil.h"
#include <string>
#include <queue>

/** Setters **/

void setData(ScpiData::Enum data, std::string value, const evpp::TCPConnPtr& conn)
{
	std::string cmd = ScpiUtil::setCmd(data, value);
	std::cout << "sending set command: " << cmd << std::endl;

	conn->Send(cmd);
}

void setData(ScpiData::Enum data, bool value, const evpp::TCPConnPtr& conn)
{
	return setData(data, std::to_string((int)value), conn);
}

void setData(ScpiData::Enum data, int value, const evpp::TCPConnPtr& conn)
{
	return setData(data, std::to_string(value), conn);
}

void setData(ScpiData::Enum data, float value, const evpp::TCPConnPtr& conn)
{
	auto s = std::to_string(value);
	s = s.substr(0, s.size()-3);
	return setData(data, s, conn);
}

void setControlMode(ControlMode::Enum value, const evpp::TCPConnPtr& conn)
{
	return setData(ScpiData::ControlMode, controlToStr(value), conn);
}

void setStartMode(StartMode::Enum value, const evpp::TCPConnPtr& conn)
{
	switch (value)
	{
		case StartMode::SafeStart:
			return setData(ScpiData::StartMode, 0, conn);
		case StartMode::AutoRestart:
			return setData(ScpiData::StartMode, 1, conn);
		default:
			return;
	}
}

/** getters **/

void getData(ScpiData::Enum data, const evpp::TCPConnPtr& conn, evpp::TCPClient& client)
{
	std::string cmd = ScpiUtil::getCmd(data);

	std::cout << "sending request command: " << cmd << std::endl;
	conn->Send(cmd);

	return;
}

void promptUser(const evpp::TCPConnPtr& conn, evpp::TCPClient& client)
{
	std::cout << "\nenter command id: " << std::flush;
	int cmd;
	std::cin >> cmd;
	switch(cmd)
	{
		case(0) : setControlMode(ControlMode::Local, conn); break;
		case(1) : setStartMode(StartMode::SafeStart, conn); break;
		case(2) : setData(ScpiData::OutputEnable, true, conn); break;
		case(3) : setData(ScpiData::OutputEnable, false, conn); break;
		case(4) : setData(ScpiData::VoltageLimit, 0.0f, conn); break;
		case(5) : setData(ScpiData::VoltageLimit, 30.0f, conn); break;
		case(6) : setData(ScpiData::CurrentLimit, 0.0f, conn); break;
		case(7) : setData(ScpiData::CurrentLimit, 50.0f, conn); break;
		case(8) : setData(ScpiData::OverVoltageLimit, 0.0f, conn); break;
		case(9) : setData(ScpiData::OverVoltageLimit, 80.0f, conn); break;
		case(10) : setData(ScpiData::UnderVoltageLimit, 0.0f, conn); break;
		case(11) : setData(ScpiData::FoldbackProtection, true, conn); break;
		case(12) : setData(ScpiData::FoldbackProtection, false, conn); break;

		case(20) : getData(ScpiData::ControlMode, conn, client); break;
		case(21) : getData(ScpiData::StartMode, conn, client); break;
		case(22) : getData(ScpiData::OverVoltageLimit, conn, client); break;
		case(23) : getData(ScpiData::UnderVoltageLimit, conn, client); break;
		case(24) : getData(ScpiData::FoldbackProtection, conn, client); break;
		case(25) : getData(ScpiData::OutputEnable, conn, client); break;
		case(26) : getData(ScpiData::OutputVoltage, conn, client); break;
		case(27) : getData(ScpiData::OutputCurrent, conn, client); break;
		case(28) : getData(ScpiData::OverVoltageTripped, conn, client); break;
		case(29) : getData(ScpiData::FoldbackTripped, conn, client); break;
		default :
		{
			std::cout << "\
					 case(0) : setControlMode(ControlMode::Local, conn); break;\n\
					 case(1) : setStartMode(StartMode::SafeStart, conn); break;\n\
					 case(2) : setData(ScpiData::OutputEnable, true, conn); break;\n\
					 case(3) : setData(ScpiData::OutputEnable, false, conn); break;\n\
					 case(4) : setData(ScpiData::VoltageLimit, 0.0f, conn); break;\n\
					 case(5) : setData(ScpiData::VoltageLimit, 30.0f, conn); break;\n\
					 case(6) : setData(ScpiData::CurrentLimit, 0.0f, conn); break;\n\
					 case(7) : setData(ScpiData::CurrentLimit, 50.0f, conn); break;\n\
					 case(8) : setData(ScpiData::OverVoltageLimit, 0.0f, conn); break;\n\
					 case(9) : setData(ScpiData::OverVoltageLimit, 80.0f, conn); break;\n\
					 case(10) : setData(ScpiData::UnderVoltageLimit, 0.0f, conn); break;\n\
					 case(11) : setData(ScpiData::FoldbackProtection, true, conn); break;\n\
					 case(12) : setData(ScpiData::FoldbackProtection, false, conn); break;\n\
\
					 case(20) : getData(ScpiData::ControlMode, conn, client); break;\n\
					 case(21) : getData(ScpiData::StartMode, conn, client); break;\n\
					 case(22) : getData(ScpiData::OverVoltageLimit, conn, client); break;\n\
					 case(23) : getData(ScpiData::UnderVoltageLimit, conn, client); break;\n\
					 case(24) : getData(ScpiData::FoldbackProtection, conn, client); break;\n\
					 case(25) : getData(ScpiData::OutputEnable, conn, client); break;\n\
					 case(26) : getData(ScpiData::OutputVoltage, conn, client); break;\n\
					 case(27) : getData(ScpiData::OutputCurrent, conn, client); break;\n\
					 case(28) : getData(ScpiData::OverVoltageTripped, conn, client); break;\n\
					 case(29) : getData(ScpiData::FoldbackTripped, conn, client); break;" << std::endl;

		}
	}
}

int main()
{
	std::string addr = "0.0.0.0:9099";

	std::cout << "starting client on: " << addr << std::endl;

	evpp::EventLoop loop;
	evpp::TCPClient client(&loop,addr,"scpiclient");

	client.Connect();

	//When getting a reply
	client.SetMessageCallback([&](const evpp::TCPConnPtr& conn, evpp::Buffer* msg)
	{
		std::cout << "\treceived reply: " << msg->NextAll().data() << std::endl;
	});

	//when first connected
	evpp::TCPConnPtr _conn;
	client.SetConnectionCallback([&](const evpp::TCPConnPtr& conn)
	{
		std::cout << "connected to server" << std::endl;
		_conn = conn;
	});

	loop.RunEvery(evpp::Duration(0.1),[&](){promptUser(_conn, client);});

	loop.Run();

	return 0;
}
