#pragma once

#include <string>
#include <map>

/***** DATA *****/

class ScpiData
{

public:
	enum Enum
	{
		VoltageLimit,
		CurrentLimit,
		OutputEnable,
		OutputVoltage,
		OutputCurrent,
		OperatingMode,
		ControlMode,
		StartMode,
		OverVoltageLimit,
		OverVoltageTripped,
		UnderVoltageLimit,
		FoldbackProtection,
		FoldbackTripped
	};

public:
	static std::string toCmd(ScpiData::Enum value);

private:
	typedef std::map<ScpiData::Enum, std::string> ScpiDataMap;
	static ScpiDataMap& getMap();
};

/***** UTIL *****/

class ScpiUtil
{
public:
	static std::string setCmd(ScpiData::Enum data, std::string value);
	static std::string getCmd(ScpiData::Enum data);
};

/***** COMMON *****/

namespace OperatingMode {
	enum Enum
	{
		Unknown				= 0,
		Off,
		ConstantVoltage,
		ConstantCurrent
	};
}

namespace ControlMode {
	enum Enum
	{
		Unknown				= 0,
		Local,
		Remote,
		LocalLockout
	};
}

namespace StartMode {
	enum Enum
	{
		Unknown				= 0,
		SafeStart,
		AutoRestart
	};
}

namespace CommandType {
    enum Enum {
	Get,
	Set
    };
}

std::string controlToStr(ControlMode::Enum value);

std::string operatingToStr(OperatingMode::Enum value);

std::string startModeToString(StartMode::Enum value);
