#include "ScpiUtil.h"

/***** DATA *****/

std::string ScpiData::toCmd(ScpiData::Enum value)
{
	return getMap()[value];
}

ScpiData::ScpiDataMap&ScpiData::getMap()
{
	static ScpiDataMap map;

	if (map.size() == 0)
	{
		map[ScpiData::VoltageLimit]			= ":VOLT";
		map[ScpiData::CurrentLimit]			= ":CURR";
		map[ScpiData::OutputEnable]			= "OUTP:STAT";
		map[ScpiData::OutputVoltage]		= "MEAS:VOLT";
		map[ScpiData::OutputCurrent]		= "MEAS:CURR";
		map[ScpiData::OperatingMode]		= "SOUR:MOD";
		map[ScpiData::ControlMode]			= "SYST:SET";
		map[ScpiData::StartMode]			= "OUTP:PON";
		map[ScpiData::OverVoltageLimit]		= ":VOLT:PROT:LEV";
		map[ScpiData::OverVoltageTripped]	= ":VOLT:PROT:TRIP";
		map[ScpiData::UnderVoltageLimit]	= ":VOLT:LIM:LOW";
		map[ScpiData::FoldbackProtection]	= ":CURR:PROT:STAT";
		map[ScpiData::FoldbackTripped]		= ":CURR:PROT:TRIP";
	}

	return map;
}

/***** UTIL *****/

std::string ScpiUtil::setCmd(ScpiData::Enum data, std::string value)
{
	return ScpiData::toCmd(data) + " " + value + "\n";
}

std::string ScpiUtil::getCmd(ScpiData::Enum data)
{
	return ScpiData::toCmd(data) + "?" + "\n";
}

std::string controlToStr(ControlMode::Enum value)
{
	switch (value)
	{
		case ControlMode::Local:
			return "LOC";
		case ControlMode::Remote:
			return "REM";
		case ControlMode::LocalLockout:
			return "LLO";
		default:
			return "";
	}
}

std::string operatingToStr(OperatingMode::Enum value)
{
	switch (value)
	{
		case OperatingMode::ConstantVoltage:
			return "CV";
		case OperatingMode::ConstantCurrent:
			return "CC";
		case OperatingMode::Off:
			return "OFF";
		default:
			return "";
	}
}

std::string startModeToString(StartMode::Enum value)
{
	switch(value)
	{
		case StartMode::SafeStart: return "ON";
		case StartMode::AutoRestart: return "ON";
		case StartMode::Unknown: return "ON";
	}
	return "UK";
}
