# Install script for directory: /home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Release")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so.0.7.0.3792"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so.0.7"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp.so.0.7.0.3792"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp.so.0.7"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so.0.7.0.3792"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so.0.7"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_static.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_lite_static.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so.0.7.0.3792"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so.0.7"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_boost.so.0.7.0.3792"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_boost.so.0.7"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so.0.7.0.3792"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so.0.7"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_boost.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_boost.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_boost_static.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so.0.7.0.3792"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so.0.7"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      file(RPATH_CHECK
           FILE "${file}"
           RPATH "")
    endif()
  endforeach()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_concurrentqueue.so.0.7.0.3792"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_concurrentqueue.so.0.7"
    )
  foreach(file
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so.0.7.0.3792"
      "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so.0.7"
      )
    if(EXISTS "${file}" AND
       NOT IS_SYMLINK "${file}")
      if(CMAKE_INSTALL_DO_STRIP)
        execute_process(COMMAND "/usr/bin/strip" "${file}")
      endif()
    endif()
  endforeach()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so"
         RPATH "")
  endif()
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE SHARED_LIBRARY FILES "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_concurrentqueue.so")
  if(EXISTS "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}${CMAKE_INSTALL_PREFIX}/lib/libevpp_concurrentqueue.so")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/lib" TYPE STATIC_LIBRARY FILES "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/lib/libevpp_concurrentqueue_static.a")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/evpp" TYPE FILE FILES
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/any.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/buffer.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/connector.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/dns_resolver.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/duration.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/duration.inl.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/event_loop.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/event_loop_thread.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/event_loop_thread_pool.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/event_watcher.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/fd_channel.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/gettimeofday.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/inner_pre.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/invoke_timer.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/libevent.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/listener.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/logging.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/memmem.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/platform_config.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/server_status.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/slice.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/sockets.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/sys_addrinfo.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/sys_sockets.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/tcp_callbacks.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/tcp_client.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/tcp_conn.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/tcp_server.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/thread_dispatch_policy.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/timestamp.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/timestamp.inl.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/utility.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/windows_port.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/evpp/http" TYPE FILE FILES
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/http/context.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/http/http_server.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/http/service.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/http/stats.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/evpp/httpc" TYPE FILE FILES
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/httpc/conn.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/httpc/conn_pool.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/httpc/request.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/httpc/response.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/httpc/ssl.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/httpc/url_parser.h"
    )
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  file(INSTALL DESTINATION "${CMAKE_INSTALL_PREFIX}/include/evpp/udp" TYPE FILE FILES
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/udp/sync_udp_client.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/udp/udp_message.h"
    "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/evpp/udp/udp_server.h"
    )
endif()

