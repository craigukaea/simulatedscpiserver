# CMake generated Testfile for 
# Source directory: /home/chickman/Git/SimulatedScpiServer/third-party/evpp
# Build directory: /home/chickman/Git/SimulatedScpiServer/third-party/evpp/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("evpp")
subdirs("apps")
subdirs("labs")
subdirs("test")
subdirs("examples")
subdirs("benchmark")
