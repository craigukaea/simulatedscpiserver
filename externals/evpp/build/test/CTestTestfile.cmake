# CMake generated Testfile for 
# Source directory: /home/chickman/Git/SimulatedScpiServer/third-party/evpp/test
# Build directory: /home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(evpp_unittest "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/bin/evpp_unittest")
add_test(evpp_unittest_concurrentqueue "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/bin/evpp_unittest_concurrentqueue")
add_test(evpp_unittest_boost_lockfree "/home/chickman/Git/SimulatedScpiServer/third-party/evpp/build/bin/evpp_unittest_boost_lockfree")
subdirs("stability")
subdirs("more_tests")
